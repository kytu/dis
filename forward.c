#include<stdlib.h>
#include<stdio.h>
#define MAX_NODE_NUM 300
#define MAX_DECEASE_NAME 1000

struct nodes{

    int id;
    int child[MAX_NODE_NUM];
    int child_length;
    int ancest[MAX_NODE_NUM];
    int ancest_length;

    //C doesn't support boolean
    int is_decease;
    int is_leaf;

    //If is_decease is true then decease_name is availalbe.
    char decease_name[MAX_DECEASE_NAME];

    //患部跟外觀
    int sym_region;
    int sym_looks;

    //forward_chaining 中被推論過的 symptom
    int inffered;

    int default_count;
    int current_count;

} node[MAX_NODE_NUM];

// agenda act like a stack, stored the id of a symptom
int agenda[MAX_NODE_NUM];
int agenda_top = 0;

int forward_chaining_initialize(){
    int i;
    for( i = 0; i < MAX_NODE_NUM; i++ ){
        node[i].inffered = 0;
        node[i].current_count = node[i].default_count;
    }
}

int forward_chaining(){
    int s, c, i;
    while(agenda_top!=0){
        s = agenda[--agenda_top];
        for( i = 0; i < node[s].ancest_length; i++ ){
            c = node[s].child[i];
            //推論過就跳過
            if(node[c].inffered) continue;

            node[c].current_count--;
            if(node[c].current_count==0){
                if(node[c].is_decease)
                    return c;
                else
                    agenda[agenda_top++] = c;
            }
        }

    }
}


int main(){

}

